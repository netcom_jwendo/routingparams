This is a simple app that could help migrating an app from Ionic 3 to Ionic 4. The app demonstrates passing data using a data service and then resolving the data after the navigation using a data resolver. 
And although there are three ways of passing data, this is the most efficient. Since we're using the two services: data service and data resolver, we will have to generate these in our ionic app:

	ionic g service services/data
	ionic g service resolver/dataResolver